﻿BMW Standart
<br>
Motociklas – dviratė motorinė transporto priemonė (su šonine priekaba arba be jos), kurios vidaus degimo variklio cilindro (cilindrų) darbinis tūris didesnis kaip 50 cm³ ir (arba) maksimalus konstrukcinis greitis didesnis kaip 45 km/h. Ši sąvoka apima ir trirates motorines transporto priemones, kurių masė be krovinio ne didesnė kaip 400 kg.