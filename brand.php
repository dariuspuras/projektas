<!DOCTYPE html>
<html>
<head>
	<title>Gamintojai</title>
	<?php include 'headerlink.html'; ?>
	<link rel="stylesheet" type="text/css" href="css/stylebrand.css">
	<script src="js/scriptbrand.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<script type="text/javascript">
			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {

				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     56],
					['Honda',      32],
					['Suzuki',    12]
					]);

				var options = {
					title: '2015 metu motociklu pardavimai'
				};

				var chart = new google.visualization.PieChart(document.getElementById('piechart'));
				chart.draw(data, options);
				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     12],
					['Honda',      76],
					['Suzuki',    43]
					]);

				var options = {
					title: '2016 metu motociklu pardavimai'
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
				chart.draw(data, options);
				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     40],
					['Honda',      22],
					['Suzuki',    15]
					]);

				var options = {
					title: '2017 metu motociklu pardavimai'
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart3'));
				chart.draw(data, options);
			}
		</script>

</head>
<body>
	<?php include 'header.html'; ?>
	<?php 

		$brandas1 = null;
		$brandas2 = null;
		$brandas3 = null;
		$t1priskirtas = false;
		$brandai = array("bmw", "honda", "suzuki");
		if(isset($_GET['tipas']) && isset($_GET['brand']))
		{
			$tipas = $_GET['tipas'];
			$brandas = $_GET['brand'];
		}
		else
		{
			$tipas = 'standart' ;
			$brandas = 'bmw';
		}
		for ($i =0 ; $i<3 ; $i++ ) {
			if($brandas == $brandai[ $i ])
			{
				$brandas1 = $brandai[ $i ];
				$t1priskirtas = true;			
			}
			else
			{
				$brandas2 = $brandai[$i];
				$brandas3 = $brandai[(($i+1)>2?0:($i+1))];
				if($t1priskirtas)$i=3;		
			}
		}
		if($brandas1=="")$brandas1=$brandai[1];
		if($tipas=="")$tipas="standart";
	// echo $brandas1.$tipas;
	?>

	<div >
		<section>
			<div class="main-box">
				<img id="big-img" class="materialboxed" src="<?php echo 'images/'.$brandas.'/'.$brandas.'_sport.jpg' ;?>">
 				<p><?php include "/txt/".$brandas."sport.txt" ;?></p>
			</div>			
		</section>

		<ul class="selection-list">
			<li>
				<div class="item-text">
					<img id="small-img" class="item" src="<?php echo 'images/'.$brandas.'/'.$brandas.'_standard.jpg' ;?>">
					<p><?php include "/txt/".$brandas."standard.txt" ;?></p>
				</div>
			</li>
			<li>
				<div class="item-text">
					<img id="small-img" class="item" src="<?php echo 'images/'.$brandas.'/'.$brandas.'_touring.jpg' ;?>">
					<p><?php include "/txt/".$brandas."touring.txt" ;?></p>
				</div>
			</li>
		</ul>

		<ul class="collapsible" data-collapsible="accordion">
		    <li>
				<div class="collapsible-header"><i class="material-icons">trending_up</i>Grafikai</div>
				
				<div class="graf collapsible-body">
					<ul class="row">
						<li class="col s12 m4 l4">
							<div id="piechart" style="width: 450px; height: 250px;"></div>
						</li>
						<li class="col s12 m4 l4">
							<div id="piechart2" style="width: 450px; height: 250px;"></div>
						</li>
						<li class="col s12 m4 l4">
							<div id="piechart3" style="width: 450px; height: 250px;"></div>
						</li>
					</ul>
				</div>
		    </li>		    
		</ul>

		<br>

		

	</div>

	<br>

	<div id="footeris">
		<br>

		<?php 
		include 'footer.html';
		?>
	</div>
</body>
</html>